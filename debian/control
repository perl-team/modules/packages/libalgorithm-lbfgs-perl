Source: libalgorithm-lbfgs-perl
Maintainer: Debian Perl Group <pkg-perl-maintainers@lists.alioth.debian.org>
Uploaders: Dima Kogan <dima@secretsauce.net>
Section: perl
Testsuite: autopkgtest-pkg-perl
Priority: optional
Build-Depends: debhelper-compat (= 13),
               perl-xs-dev,
               perl:native,
               libtest-number-delta-perl,
               libtest-pod-perl,
               liblbfgs-dev,
               libmodule-install-perl
Standards-Version: 3.9.6
Vcs-Browser: https://salsa.debian.org/perl-team/modules/packages/libalgorithm-lbfgs-perl
Vcs-Git: https://salsa.debian.org/perl-team/modules/packages/libalgorithm-lbfgs-perl.git
Homepage: https://metacpan.org/release/Algorithm-LBFGS

Package: libalgorithm-lbfgs-perl
Architecture: any
Depends: ${misc:Depends},
         ${perl:Depends},
         ${shlibs:Depends}
Description: Perl interface to an L-BFGS non-linear optimization algorithm
 Algorithm::LBFGS is an interface to liblbfgs, a C implementation of L-BFGS.
 .
 L-BFGS (Limited-memory Broyden-Fletcher-Goldfarb-Shanno) is a quasi-Newton
 method for unconstrained optimization. This method is especially efficient on
 problems involving a large number of variables.
 .
 Generally, it solves a problem described as following:
 .
 min f(x), x = (x1, x2, ..., xn)
